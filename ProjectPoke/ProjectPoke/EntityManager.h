#pragma once
#include "BaseManager.h"
#include "Player.h"
#include "SFML/Graphics/RenderWindow.hpp"
#include "NpcLoader.h"
class EntityManager : public BaseManager 
{
public:
    EntityManager();
    ~EntityManager();
    e_ManagerType getManagerType();
    void DrawMap(sf::RenderWindow& a_Window, sf::View& a_Camera);
    void Initialize();
    void Update(float a_DeltaTime);
    void AddNpcByID(int a_ID, int a_xPos, int a_yPos);
    void RunNpcScriptByID(int a_ID);
    Player player;
private:
    std::vector<Npc*> m_Npcs;
    NpcLoader* m_NpcLoader;
};