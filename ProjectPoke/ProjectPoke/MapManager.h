#pragma once
#include "BaseManager.h"
#include "TileMap.h"
#include "MapLoader.h"
#include "MovementDirectionEnum.h"
class MapManager : public BaseManager
{
public:
	MapManager();
	~MapManager();
	e_ManagerType getManagerType() override;
	void Initialize() override;
	void DrawMap(sf::RenderWindow& a_Window);
	bool tileIsWalkable(int xPos, int yPos);
	bool hasEventOnTile(int xPos, int yPos);
	bool hasNpcAtDirection(MovementDirection  a_Direction);
	void runNpcScriptAtDirection(MovementDirection a_Direction);
	void runEventOnTile(int xPos, int yPos);
private:
	TileMap* m_CurTileMap;
	MapLoader* m_MapLoader;
};