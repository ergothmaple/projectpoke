#include <SFML/Graphics.hpp>
#include "Tile.h"
class DrawTileMap : public sf::Drawable, public sf::Transformable
#pragma once
{
public:
	DrawTileMap();
	~DrawTileMap();
	std::string m_TexturePath;
	bool load(const std::string& texturePath, sf::Vector2u tileSize, std::vector<Tile*> tilesToBeDrawn, int width, int height);
private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	sf::VertexArray m_vertices;
	sf::Texture m_tileset;
};

