#include "ManagerManager.h"
ManagerManager* ManagerManager::instance = nullptr;

void ManagerManager::addManager(BaseManager* a_Manager)
{
	m_Managers.push_back(a_Manager);
}

std::vector<BaseManager*> ManagerManager::getManagersOfType(e_ManagerType a_ManagerType)
{
	std::vector<BaseManager*> returnVec;
	//Loop through the whole vector to find managers of said type.
	//Add managers of said type to the vector that should be returned
	for (int i = 0; i < m_Managers.size(); i++)
	{
		if (m_Managers.at(i)->getManagerType() == a_ManagerType)
		{
			returnVec.push_back(m_Managers.at(i));
		}
	}
	return returnVec;
}

void ManagerManager::InitializeManagers()
{
	for (int i = 0; i < m_Managers.size(); i++)
	{
		m_Managers.at(i)->Initialize();
	}
}

ManagerManager* ManagerManager::GetInstance()
{
	if (instance == 0)
	{
		instance = new ManagerManager;
	}
	return instance;
};
