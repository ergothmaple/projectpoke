#include "Npc.h"
Npc::Npc()
{
}

Npc::~Npc()
{
}

void Npc::SetDirection(MovementDirection a_MovementDirection)
{
    m_MovementDirection = a_MovementDirection;
    if (m_MovementDirection == MovementDirection::Right)
    {
        m_Sprite.setTextureRect(sf::IntRect(32, 64, -16, 32));
    }
    if (m_MovementDirection == MovementDirection::Left)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 64, 16, 32));
    }
    if (m_MovementDirection == MovementDirection::Up)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 32, 16, 32));
    }
    if (m_MovementDirection == MovementDirection::Down)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 0, 16, 32));
    }
}

void Npc::Initialize()
{
    if (m_Texture.loadFromFile(m_TexturePath))
    {
        m_Sprite.setTexture(m_Texture);
        m_Sprite.setScale(2.f, 2.f);
        m_Sprite.setPosition(m_xPos, m_yPos);
    }
    else
    {
        printf("Can't load texture \r\n");
    }
    if (m_MovementDirection == MovementDirection::Right)
    {
        m_Sprite.setTextureRect(sf::IntRect(32, 64, -16, 32));
    }
    if (m_MovementDirection == MovementDirection::Left)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 64, 16, 32));
    }
    if (m_MovementDirection == MovementDirection::Up)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 32, 16, 32));
    }
    if (m_MovementDirection == MovementDirection::Down)
    {
        m_Sprite.setTextureRect(sf::IntRect(16, 0, 16, 32));
    }
    m_ScriptName = "Npc_0" + std::to_string(m_ID);
}
