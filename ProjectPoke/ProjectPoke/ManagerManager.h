#pragma once
#include "BaseManager.h"
#include <vector>
#include <iostream>
class ManagerManager
{
public:
	~ManagerManager() {};
	void addManager(BaseManager* a_Manager);
	std::vector<BaseManager*> getAllManagers() { return m_Managers; };
	std::vector<BaseManager*> getManagersOfType(e_ManagerType a_ManagerType);
	void InitializeManagers();

	static ManagerManager* GetInstance();
private:
	ManagerManager() {};
	std::vector<BaseManager*> m_Managers;
	static ManagerManager* instance;
};

