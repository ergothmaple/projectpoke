#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "MovementDirectionEnum.h"
class Npc
{
public:
	Npc();
	~Npc();
	int m_ID;
	int m_xPos;
	int m_yPos;
	MovementDirection m_MovementDirection;
	void SetDirection(MovementDirection a_MovementDirection);
	std::string m_TexturePath;
	sf::Sprite m_Sprite;
	sf::Texture m_Texture;
	std::string m_ScriptName;
	void Initialize();
private:
};
