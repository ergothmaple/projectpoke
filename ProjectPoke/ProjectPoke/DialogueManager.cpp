#include "DialogueManager.h"
#include "ManagerManager.h"
#include "ScriptEventManager.h"
#include "EntityManager.h"
DialogueManager::DialogueManager()
{
}

DialogueManager::~DialogueManager()
{
}

e_ManagerType DialogueManager::getManagerType()
{
    return e_ManagerType::DialogueManager;
}

void DialogueManager::Initialize()
{
    if (!m_Font.loadFromFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Fonts/Pokemon.ttf"))
    {
        printf("Can't load font \r\n");
    }
    m_Text.setFont(m_Font);
    m_Text.setFillColor(sf::Color::Black);
    m_Text.setCharacterSize(25);
    m_Text.setPosition(-220, -100);
    m_Text.setLineSpacing(0.8f);
    m_OptionText = m_Text;
    if (!m_DialogueBoxTex.loadFromFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Sprites/UI/DialogueText.png"))
    {
        printf("Can't load Texture \r\n");
    }
    m_DialogueBox.setTexture(m_DialogueBoxTex);
    m_DialogueBox.setPosition(-220, -100);
    m_DialogueBox.setScale(2, 2);

    m_DialogueOptionBox.setTexture(m_DialogueBoxTex);
    m_DialogueOptionBox.setPosition(-220, -100);
    m_DialogueOptionBox.setScale(.5f, .5f);
    if (!m_PointerTex.loadFromFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Sprites/UI/Pointer.png"))
    {
        printf("Can't load Texture \r\n");
    }
    m_Pointer.setTexture(m_PointerTex);
    m_Pointer.setScale(2.f, 2.f);

}

void DialogueManager::DisplayText(sf::RenderWindow& a_Window, sf::View& a_Camera, float a_DeltaTime)
{
    if (m_IsActive)
    {
        m_PointerMoveTimer -= a_DeltaTime;
        if (m_PointerMoveTimer <= 0)
        {
            m_PointerMoveTimer = .5f;
            m_PointerIsUp == true ? m_PointerIsUp = false : m_PointerIsUp = true;
        }
        m_DialogueBox.setPosition(a_Camera.getCenter().x - a_Camera.getSize().x / 2.f + 7.f, a_Camera.getCenter().y + a_Camera.getSize().y / 3.2f);
        m_Text.setPosition(m_DialogueBox.getPosition().x + 20.f, m_DialogueBox.getPosition().y + 5.f);
        m_Pointer.setPosition(m_DialogueBox.getPosition().x + m_DialogueBox.getLocalBounds().width * 2.f - 25.f, m_DialogueBox.getPosition().y + m_DialogueBox.getLocalBounds().height * 2.f - 25.f - 3.f * m_PointerIsUp);
        a_Window.draw(m_DialogueBox);
        a_Window.draw(m_Text);
        if (m_DialogueType == e_DialogueType::OptionDialogue)
        {
            m_DialogueOptionBox.setPosition(a_Camera.getCenter().x + a_Camera.getSize().x / 2.f - 160.f, a_Camera.getCenter().y + a_Camera.getSize().y / 3.3f - 42.f * m_DialogueOptionBox.getScale().y);
            m_OptionText.setPosition(m_DialogueOptionBox.getPosition().x + 20.f, m_DialogueOptionBox.getPosition().y + 4.f);
            a_Window.draw(m_DialogueOptionBox);
            a_Window.draw(m_OptionText);
            m_Pointer.setRotation(-90.f);
            m_Pointer.setPosition(m_DialogueOptionBox.getPosition().x + 2.f + 3.f * m_PointerIsUp, m_DialogueOptionBox.getPosition().y + 25.f + m_SelectedOption * m_OptionText.getCharacterSize() - m_SelectedOption * 3.f);
            a_Window.draw(m_Pointer);
        }

        if (m_DialogueType == e_DialogueType::BaseDialogue)
        {
            m_Pointer.setRotation(0.f);
            a_Window.draw(m_Pointer);
        }
    }
}

void DialogueManager::DisplayTextOptions(std::string a_Text, std::vector<std::string> a_Options)
{
    m_DialogueOptionBox.setScale(1.3f, 1.2f * a_Options.size() / 2.f);
    SetText(a_Text);
    std::string optionString;
    for (int i = 0; i < a_Options.size(); i++)
    {
        optionString += a_Options[i];
        optionString += "\r\n";
    }
    m_OptionAmount = a_Options.size();
    m_OptionText.setString(optionString);
    m_SelectedOption = 0;
}

void DialogueManager::SetText(std::string a_Text)
{
    if (a_Text.length() > m_StringSize)
    {
        size_t pos = a_Text.rfind(" ", m_StringSize);
        if (pos != std::string::npos)
        {
            a_Text.insert(pos, "\r\n");
            a_Text.erase(pos + 2, 1);
        }
    }
    if (a_Text.length() > m_StringSize * 2 + 2)
    {
        if (a_Text.length() > m_StringSize)
        {
            size_t pos = a_Text.rfind(" ", m_StringSize * 2 + 2);
            if (pos != std::string::npos)
            {
                a_Text.insert(pos, "\r\n");
                a_Text.erase(pos + 2, 1);
            }
        }
    }
    if (a_Text.length() > m_StringSize * 3 + 3)
    {
        if (a_Text.length() > m_StringSize)
        {
            size_t pos = a_Text.rfind(" ", m_StringSize * 3 );
            if (pos != std::string::npos)
            {
                a_Text.insert(pos, "\r\n");
                a_Text.erase(pos + 2, 1);
            }
        }
    }
    m_Text.setString(a_Text);
}

void DialogueManager::ContinuePressed()
{
    m_IsActive = false;
    m_OptionAmount = 0;
    m_SelectedOption = 0;
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementEnabled = true;
    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager)[0])->RunNextLine();
}
