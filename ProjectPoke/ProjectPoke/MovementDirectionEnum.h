#pragma once
enum class MovementDirection
{
	None,
	Up,
	Down,
	Left,
	Right
};