#include "NpcLoader.h"
#include <fstream>
#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include <rapidjson/istreamwrapper.h>
NpcLoader::NpcLoader()
{
}

NpcLoader::~NpcLoader()
{
}

void NpcLoader::LoadNpcData(Npc* a_Npc)
{
    //Load and parse file
    std::ifstream myFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Npcs/"+ std::to_string(a_Npc->m_ID) + ".json");
    rapidjson::IStreamWrapper WrappedFile(myFile);
    if (!myFile.is_open())
    {
        printf("[ERROR] Couldn't find Npc file with path %s \r\n", std::to_string(a_Npc->m_ID).c_str());
        return ;
    }
    rapidjson::Document doc;
    if (doc.ParseStream(WrappedFile).HasParseError())
    {
        printf("[ERROR] Parse error while parsing the Npc data file.\r\n");
        return ;
    }
    if (doc.HasMember("MovementDirection"))
    {
        a_Npc->m_MovementDirection = static_cast<MovementDirection>(doc.FindMember("MovementDirection")->value.GetInt());
    }
    if (doc.HasMember("TexturePath"))
    {
        a_Npc->m_TexturePath = doc.FindMember("TexturePath")->value.GetString();
    }
    if (doc.HasMember("ScriptName"))
    {
        a_Npc->m_ScriptName = doc.FindMember("ScriptName")->value.GetString();
    }
}
