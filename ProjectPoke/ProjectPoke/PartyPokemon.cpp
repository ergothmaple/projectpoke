#include "PartyPokemon.h"

PartyPokemon::PartyPokemon()
{
}

PartyPokemon::~PartyPokemon()
{
}

void PartyPokemon::AddPokemon(Pokemon* a_Pokemon)
{
     m_PartyPokemon.push_back(a_Pokemon);
}

void PartyPokemon::LoadAndDrawPokemon()
{
    for (Pokemon* pokemon : m_PartyPokemon)
    {
        pokemon->LoadPokemonSprite();
    }
}

void PartyPokemon::UpdatePokemon(float a_DeltaTime)
{
    for (Pokemon* pokemon : m_PartyPokemon)
    {
        pokemon->Update(a_DeltaTime);
    }
}

std::vector<Pokemon*> PartyPokemon::GetPartyPokemon()
{
    return m_PartyPokemon;
}
