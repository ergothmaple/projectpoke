#pragma once
#include "BaseManager.h"
#include <string>
#include "FileManager.h"
#include "ScriptParser.h"
#include "FunctionManager.h"
#include "ScriptableFunction.h"
class ScriptEventManager : public BaseManager
{
public:
	ScriptEventManager();
	~ScriptEventManager();
	e_ManagerType getManagerType() override;
	void Initialize() override;
	void StartScript(std::string a_ScriptName);
	void RestartScriptFromCurrentLine();
	int m_Index = 0;
	void RunNextLine();
	void FinishScript();
	bool m_IsRunningScript = false;
	FunctionManager& getFunctionManager();
private:
	const FileManager fileManager;
	const ScriptParser scriptParser;
	FunctionManager functionManager;
	std::string m_CurrentScriptName;

};