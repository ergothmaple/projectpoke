#pragma once
#include "Pokemon.h"
#include <vector>
class PokemonLoader
{
public:
	PokemonLoader();
	~PokemonLoader();
	void LoadPokemon(std::vector<Pokemon*>& a_Pokemon);
	bool IsValidPokemon(Pokemon* a_Pokemon);
private:

};