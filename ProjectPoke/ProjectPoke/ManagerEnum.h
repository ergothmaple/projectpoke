#pragma once
 enum class e_ManagerType {
	BaseManager= 1,
	MapManager = 2,
    InputManager = 3,
    EntityManager = 4,
    ScriptEventManager = 5,
    DialogueManager = 6,
    PokemonManager = 7,
    MenuUIManager = 8
 };