#include "PlayerPokemonHolder.h"

PlayerPokemonHolder::PlayerPokemonHolder()
{
}

PlayerPokemonHolder::~PlayerPokemonHolder()
{
}

void PlayerPokemonHolder::AddPokemonToParty(Pokemon* a_Pokemon)
{
    m_PartyPokemon.AddPokemon(a_Pokemon);
}

void PlayerPokemonHolder::LoadAndDrawPartyPokemon()
{
    m_PartyPokemon.LoadAndDrawPokemon();
}

void PlayerPokemonHolder::UpdatePokemon(float a_DeltaTime)
{
    m_PartyPokemon.UpdatePokemon(a_DeltaTime);
}

std::vector<Pokemon*> PlayerPokemonHolder::GetPartyPokemon()
{
    return m_PartyPokemon.GetPartyPokemon();
}
