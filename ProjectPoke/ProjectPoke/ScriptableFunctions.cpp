#include "ScriptableFunctions.h"
#include <string>
#include <iostream>
#include "ManagerManager.h"
#include "DialogueManager.h"
#include "EntityManager.h"
#include "ScriptEventManager.h"
#include "PokemonManager.h"

void ScriptableFunctions::MovePlayerX(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto x = std::stoi(v1[0]);
    if (x > 0)
    {
        static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementDirection = MovementDirection::Right;
    }
    else if (x < 0)
    {
        static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementDirection = MovementDirection::Left;
    }
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.MovePlayer(x, 0);
}

void ScriptableFunctions::MovePlayerY(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto y = std::stoi(v1[0]);
    if (y > 0)
    {
        static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementDirection = MovementDirection::Down;
    }
    else if (y < 0)
    {
        static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementDirection = MovementDirection::Up;
    }
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.MovePlayer(0, y);
}

void ScriptableFunctions::SayDialogue(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto dialogueText = v1[0];
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->m_IsActive = true;
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->SetText(dialogueText);
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->m_DialogueType = e_DialogueType::BaseDialogue;
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementEnabled = false;

}

void ScriptableFunctions::SayOptionDialogue(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto optionAmount = std::stoi(v1[0]);
    const auto Dialogue = v1[1];
    std::vector<std::string> options;
    int index = 0;

    for (int i = 0; i < optionAmount; i++)
    {

        std::string optionStart = "#" + std::to_string(index) + "#";
        std::string option;
        std::string optionEnd;
        if (i + 1 < optionAmount)
        {
            optionEnd = "#" + std::to_string(index + 1) + "#";
            option = Dialogue.substr(Dialogue.find(optionStart), Dialogue.find(optionEnd) - Dialogue.find(optionStart));
        }
        else
        {
            option = Dialogue.substr(Dialogue.find(optionStart), Dialogue.size());
        }
        option.erase(0, 3);
        options.push_back(option);
        index++;
    }
    std::string dialogue = Dialogue.substr(0, Dialogue.find("#"));
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->m_IsActive = true;
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->m_DialogueType = e_DialogueType::OptionDialogue;
    static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->DisplayTextOptions(dialogue, options);
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_MovementEnabled = false;
}

void ScriptableFunctions::SetVariable(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto Name = v1[0];
    const auto Value = v1[1];
    printf("Variable Key: %s, Set to Value: %s \r\n", Name.c_str(), Value.c_str()); 
    if (static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->getFunctionManager().variableMap.count(Name) == 0)
    {
        static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->getFunctionManager().variableMap[Name] = Value;
    }
    else
    {
        static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->getFunctionManager().variableMap[Name] = Value;
        auto variableMap = static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->getFunctionManager().variableMap;
    }
    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->m_Index++;
    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->RestartScriptFromCurrentLine();
}

void ScriptableFunctions::GivePokemon(std::vector<std::string> args)
{
    auto v1 = static_cast<std::vector<std::string>*>(&args)[0];
    const auto pokemonID = std::stoi(v1[0]);
    auto PokemonToBeCopied = static_cast<PokemonManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::PokemonManager).at(0))->GetPokemonByID(pokemonID);
    if (PokemonToBeCopied != nullptr)
    {
        Pokemon* pokemon = new Pokemon(*PokemonToBeCopied);
        static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_PokemonHolder.AddPokemonToParty(pokemon);
    }
    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->m_Index++;
    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->RunNextLine();
}

