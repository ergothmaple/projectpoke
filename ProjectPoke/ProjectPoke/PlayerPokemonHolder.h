#pragma once
#include "PartyPokemon.h"
#include "BoxPokemon.h"
class PlayerPokemonHolder
{
public:
	PlayerPokemonHolder();
	~PlayerPokemonHolder();
	void AddPokemonToParty(Pokemon* a_Pokemon);
	void LoadAndDrawPartyPokemon();
	void UpdatePokemon(float a_DeltaTime);
	std::vector<Pokemon*> GetPartyPokemon();
private:
	PartyPokemon m_PartyPokemon;
	BoxPokemon m_BoxPokemon;
};