#include "pokemonManager.h"
#include <stdio.h>
PokemonManager::PokemonManager()
{
}

PokemonManager::~PokemonManager()
{
}

e_ManagerType PokemonManager::getManagerType()
{
    return e_ManagerType::PokemonManager;
}

void PokemonManager::Initialize()
{
    printf(" Load pokeymans \r\n");
    m_PokemonLoader.LoadPokemon(m_Pokemon);

    for (Pokemon* pokemon : m_Pokemon)
    {
        pokemon->LoadPokemonSprite();
    }
}

Pokemon* PokemonManager::GetPokemonByID(const int a_PokemonID)
{
    for (Pokemon* pokemon : m_Pokemon)
    {
        if (pokemon->m_ID == a_PokemonID)
        {
            return pokemon;
        }
    }
    return nullptr;
}

void PokemonManager::DrawMap(sf::RenderWindow& a_Window, sf::View& a_Camera)
{
    for (Pokemon* pokemon : m_Pokemon)
    {
        if (pokemon->m_BoxTexturePath != "")
        {
            pokemon->m_BoxSprite.setPosition(a_Camera.getCenter());
            a_Window.draw(pokemon->m_BoxSprite);
        }
    }
}
