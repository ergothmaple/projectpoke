#include "PokemonLoader.h"
#include <fstream>
#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include <rapidjson/istreamwrapper.h>

PokemonLoader::PokemonLoader()
{
}

PokemonLoader::~PokemonLoader()
{
}

void PokemonLoader::LoadPokemon(std::vector<Pokemon*>& a_Pokemon)
{
    std::vector<int> PokeID;

    //Load Pokedex file to get Id's of all pokemon in order.
    printf(" Starting to load pokermans \r\n");
    std::ifstream myFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Pokemon/Pokedex.json");
    rapidjson::IStreamWrapper WrappedFile(myFile);
    if (!myFile.is_open())
    {
        printf("[ERROR] Couldn't find PokeDex file with path %s \r\n", "C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Pokemon/Pokedex.json");
        return;
    }

    rapidjson::Document doc;
    if (doc.ParseStream(WrappedFile).HasParseError())
    {
        printf("[ERROR] Parse error while parsing the PokeDex data file.\r\n");
        return;
    }
    if (doc.HasMember("IdList"))
    {
        auto value = doc.FindMember("IdList")->value.GetArray();
        int Size = value.Size();
        for (int i = 0; i < Size; i++)
        {
            PokeID.push_back(value[i].GetInt());
        }
    }

    for (int i = 0; i < PokeID.size(); i++)
    {
        std::ifstream myFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Pokemon/Pokemon/" + std::to_string(PokeID.at(i)) + ".json");
        rapidjson::IStreamWrapper WrappedFile(myFile);
        if (!myFile.is_open())
        {
            printf("[ERROR] Couldn't find Pokemon file with path %s \r\n", "C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Pokemon/Pokedex.json");
            return;
        }

        rapidjson::Document doc;
        if (doc.ParseStream(WrappedFile).HasParseError())
        {
            printf("[ERROR] Parse error while parsing the Pokemon data file with ID %s.\r\n", std::to_string(PokeID.at(i)));
            return;
        }
        //Fill in pokemon data
        Pokemon* pokemon = new Pokemon();
        if (doc.HasMember("Id"))
        {
            pokemon->m_ID = doc.FindMember("Id")->value.GetInt();
        }
        if (doc.HasMember("Name"))
        {
            pokemon->m_Name = doc.FindMember("Name")->value.GetString();
        }
        if (doc.HasMember("Level"))
        {
            pokemon->m_Level = doc.FindMember("Level")->value.GetInt();
        }
        if (doc.HasMember("HP"))
        {
            pokemon->m_MaxHP = doc.FindMember("HP")->value.GetInt();
        }
        if (doc.HasMember("Gender"))
        {
            pokemon->m_Gender = doc.FindMember("Gender")->value.GetInt();
        }
        if (doc.HasMember("BoxSpritePath"))
        {
            pokemon->m_BoxTexturePath = doc.FindMember("BoxSpritePath")->value.GetString();
        }
        if (IsValidPokemon(pokemon))
        {
            pokemon->LoadPokemonSprite();
            pokemon->InitializeData();
            a_Pokemon.push_back(pokemon);
        }
    }
}

bool PokemonLoader::IsValidPokemon(Pokemon* a_Pokemon)
{
    if (a_Pokemon->m_ID != 0 && a_Pokemon->m_Name != "" && a_Pokemon->m_Level != 0 && a_Pokemon->m_MaxHP != 0 && a_Pokemon->m_BoxTexturePath != "")
    {
        return true;
    }
    return false;
}
