#pragma once
#include "ManagerEnum.h"
#include "BaseManager.h"
#include "PokemonLoader.h"
#include "SFML/Graphics/RenderWindow.hpp"
class PokemonManager : public BaseManager
{
public:
	PokemonManager();
	~PokemonManager();
	e_ManagerType getManagerType();
	void Initialize();
	Pokemon* GetPokemonByID(const int a_PokemonID);
	void DrawMap(sf::RenderWindow& a_Window, sf::View& a_Camera);
protected:
private:
	PokemonLoader m_PokemonLoader;
	std::vector<Pokemon*> m_Pokemon;
};