#include "DrawTileMap.h"

DrawTileMap::DrawTileMap()
{
}

DrawTileMap::~DrawTileMap()
{
}

bool DrawTileMap::load(const std::string& texturePath, sf::Vector2u tileSize, std::vector<Tile*> tilesToBeDrawn, const int width, const int height)
{
    //Load tileset texture
    if (!m_tileset.loadFromFile(texturePath))
    {
        printf("Couldn't load texture file \r\n");
        return false;
    }
    //resize vertex array to fit the proper amount of tiles
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(tilesToBeDrawn.size() * 4);

    for (int i = 0; i < tilesToBeDrawn.size(); i++)
    {
        // find its position in the tileset texture
        int tu = tilesToBeDrawn[i]->m_TextureXY.x * 32;
        int tv = tilesToBeDrawn[i]->m_TextureXY.y * 32;
        if (tu == -32 && tv == -32)
        {
        }
        else
        {
            // get a pointer to the current tile's quad
            sf::Vertex* quad = &m_vertices[i * 4];
            int x = i % width;
            int y = i / width;
            // define its 4 corners (worldspace)
            quad[0].position = sf::Vector2f(tilesToBeDrawn[i]->m_Position.x, tilesToBeDrawn[i]->m_Position.y);
            quad[1].position = sf::Vector2f(tilesToBeDrawn[i]->m_Position.x + 32, tilesToBeDrawn[i]->m_Position.y);
            quad[2].position = sf::Vector2f(tilesToBeDrawn[i]->m_Position.x + 32, tilesToBeDrawn[i]->m_Position.y + 32);
            quad[3].position = sf::Vector2f(tilesToBeDrawn[i]->m_Position.x, tilesToBeDrawn[i]->m_Position.y + 32);

            // define its 4 texture coordinates (texture uv)
            quad[0].texCoords = sf::Vector2f(tu, tv);
            quad[1].texCoords = sf::Vector2f((tu + 32), tv);
            quad[2].texCoords = sf::Vector2f((tu + 32), (tv + 32));
            quad[3].texCoords = sf::Vector2f(tu, (tv + 32));
        }
    }
    return true;
}

void DrawTileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{

    //apply transform
    states.transform *= getTransform();
    //apply tileset texture
    states.texture = &m_tileset;
    //draw vertex array
    target.draw(m_vertices, states);
}
