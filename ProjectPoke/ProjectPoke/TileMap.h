#pragma once
#include <iostream>
#include <vector>
#include "Layer.h"
#include "SFML/Graphics/RenderWindow.hpp"
class TileMap
{
public:
	TileMap();
	~TileMap();
	
	void ResizeTileMap();
	void loadTileSprites();
	void DrawTileMaps(sf::RenderWindow& a_Window);

public:
	int m_Height;
	int m_Width;
	std::string m_AudioPath;
	int m_Weather; //Make enum from this later
	std::string m_MapName;
	bool m_Inside;
	int m_LayerAmount;
	std::vector<Layer*> m_Layers;
private:

};