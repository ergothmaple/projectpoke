#include "Pokemon.h"

Pokemon::Pokemon()
{
}

Pokemon::~Pokemon()
{
}

void Pokemon::LoadPokemonSprite()
{
    if (m_BoxTexture.loadFromFile(m_BoxTexturePath))
    {
        m_BoxSprite.setTexture(m_BoxTexture);
        m_BoxSprite.setScale(1.5f, 1.5f);
    }
}

void Pokemon::InitializeData()
{
    m_CurHP = 8;
    m_Level = 47;
}

void Pokemon::Update(float a_DeltaTime)
{
    m_CurrentUpdate -= a_DeltaTime;
    if (m_CurrentUpdate < 0.f)
    {
        m_CurrentUpdate = m_Countdown;
        m_BoxSpriteAnimation == 0 ? m_BoxSpriteAnimation = 1 : m_BoxSpriteAnimation = 0;
    }
}

void Pokemon::DrawIcon(sf::Vector2f a_Position, sf::RenderWindow& a_Window)
{
    m_BoxSprite.setPosition(a_Position);
    m_BoxSprite.setScale(1.f, 1.f);
    m_BoxSprite.setTextureRect(sf::IntRect(64 * m_BoxSpriteAnimation, 0 , 64 + 64 * m_BoxSpriteAnimation, 64));
    a_Window.draw(m_BoxSprite);
}
