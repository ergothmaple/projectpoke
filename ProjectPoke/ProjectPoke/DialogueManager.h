#pragma once
#include "BaseManager.h"
#include <string>
#include <SFML/Graphics.hpp>
#include "DialogueType.h"
class DialogueManager : public BaseManager
{
public:
	DialogueManager();
	~DialogueManager();
	e_ManagerType getManagerType() override;
	void Initialize() override;
	void DisplayText(sf::RenderWindow& a_Window, sf::View& a_Camera, float a_DeltaTime);
	void DisplayTextOptions(std::string a_Text, std::vector<std::string> a_Options);
	void SetText(std::string a_Text);
	bool m_IsActive = false;
	e_DialogueType m_DialogueType;
	int m_OptionAmount = 0;
	int m_SelectedOption = 0;

	//Player pressed next during the dialogue line
	void ContinuePressed();
private:
	sf::Font m_Font;
	sf::Text m_Text;
	sf::Texture m_DialogueBoxTex;
	sf::Sprite m_DialogueBox;
	sf::Sprite m_DialogueOptionBox;
	sf::Text m_OptionText;
	sf::Texture m_PointerTex;
	sf::Sprite m_Pointer;
	bool m_PointerIsUp = false;
	float m_PointerMoveTimer = .5f;

	int m_StringSize = 48;
};