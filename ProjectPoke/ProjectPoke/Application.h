#pragma once
#include <SFML/Graphics.hpp>
class Application
{
public:
	Application();
	~Application();
	void Run();
	void Initialize();
	void Update(float a_DeltaTime);
	void Draw(sf::RenderWindow& a_Window, sf::View& a_Camera);
private:
	float deltaTime;
};

