#include "InputManager.h"
#include "ManagerManager.h"
#include "EntityManager.h" 
#include "Dialoguemanager.h"
#include "MapManager.h"
#include "DialogueType.h"
#include "MenuUIManager.h"
#include "DirectionEnum.h"
InputManager::InputManager()
{
}

InputManager::~InputManager()
{
}

e_ManagerType InputManager::getManagerType()
{
    return e_ManagerType::InputManager;
}

void InputManager::Initialize()
{
}

void InputManager::ProcessInput()
{
    if (static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementEnabled)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
        {
            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.MovePlayer(0, -1);
        }
        else   if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
        {
            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.MovePlayer(-1, 0);
        }
        else   if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
        {
            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.MovePlayer(0, 1);
        }
        else   if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
        {
            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.MovePlayer(1, 0);
        }
    }
}

void InputManager::ProcessKeyReleased(sf::Event m_Event)
{
    if (m_Event.key.code == sf::Keyboard::Space)
    {
        if (static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0])->m_IsActive)
        {
            static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0])->ContinuePressed();
        }
        else
        {
            //Check if there's an npc at the direction the player is looking in
            if (static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager)[0])->hasNpcAtDirection(static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementDirection))
            {
                static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager)[0])->runNpcScriptAtDirection(static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementDirection);
            }
            else
            {
                printf("No npc found");
            }

        }
    }
    else if (m_Event.key.code == sf::Keyboard::E)
    {
        //FlipFlop between menu should/shouldn't be shown
        static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateSelectIndex(Direction::None);
        static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateMenu();
        static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn == true ? static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementEnabled = true : static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.m_MovementEnabled = false;
        static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn == true ? static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn = false : static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn = true;
        
    }

    else if (m_Event.key.code == sf::Keyboard::W)
    {
        DialogueManager* dialogueManager = static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0]);
        if (dialogueManager->m_IsActive && dialogueManager->m_DialogueType == e_DialogueType::OptionDialogue)
        {
            if (dialogueManager->m_SelectedOption - 1 < 0)
            {
                static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0])->m_SelectedOption = dialogueManager->m_OptionAmount - 1;
            }
            else
            {
                dialogueManager->m_SelectedOption -= 1;
            }
        }
        //Move through Party menu
        else if (static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn)
        {
            static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateSelectIndex(Direction::Up);
        }
    }
    else if (m_Event.key.code == sf::Keyboard::S)
    {
        DialogueManager* dialogueManager = static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0]);
        if (dialogueManager->m_IsActive && dialogueManager->m_DialogueType == e_DialogueType::OptionDialogue)
        {
            if (dialogueManager->m_SelectedOption + 1 > dialogueManager->m_OptionAmount - 1)
            {
                static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager)[0])->m_SelectedOption = 0;
            }
            else
            {
                dialogueManager->m_SelectedOption += 1;
            }
        }
        //Move through Party menu
        else if (static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn)
        {
            static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateSelectIndex(Direction::Down);
        }
    }
    else if (m_Event.key.code == sf::Keyboard::A)
    {
        //Move through Party menu
        if (static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn)
        {
            static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateSelectIndex(Direction::Left);
        }
    }
    else if (m_Event.key.code == sf::Keyboard::D)
    {
        //Move through Party menu
        if (static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->b_MenuShouldBeDrawn)
        {
            static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager)[0])->UpdateSelectIndex(Direction::Right);
        }
    }
}
