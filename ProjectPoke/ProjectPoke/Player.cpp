#include "Player.h"
#include <math.h>
#include "ManagerManager.h"
#include "MapManager.h"
#include "ScriptEventManager.h"
Player::Player()
{
};

Player::~Player()
{
}

void Player::MovePlayer(int xAmount, int yAmount)
{
    xMoveAmount = xAmount;
    yMoveAmount = yAmount;
    //set is player moving
    if (!m_IsMoving)
    {
        if (xAmount != 0)
        {
            //Move left
            if (xAmount < 0)
            {
                if (m_MovementDirection == MovementDirection::Left && static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->tileIsWalkable(xPos / 32.f - 1, yPos / 32.f + 1))
                {
                    m_OldXPos = xPos;
                    m_OldYPos = yPos;
                    xPos -= 32.f;
                    m_Timer = m_MoveSpeed;
                    m_TileSprite.setTextureRect(sf::IntRect(32, 64, -16, 32));
                    m_IsMoving = true;

                    if (m_IsLeftAnim)
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(0, 64, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                    else
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(32, 64, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                }
                else
                {
                    m_IsTurning = true;
                    m_Timer = m_TurnSpeed;
                    m_TileSprite.setTextureRect(sf::IntRect(32, 64, 16, 32));
                    m_MovementDirection = MovementDirection::Left;
                    m_IsMoving = true;
                }
            }
            //Move right
            else
            {
                if (m_MovementDirection == MovementDirection::Right && static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->tileIsWalkable(xPos / 32.f + 1, yPos / 32.f + 1))
                {
                    m_OldXPos = xPos;
                    m_OldYPos = yPos;
                    xPos += 32.f;
                    m_Timer = m_MoveSpeed;
                    m_IsMoving = true;
                    if (m_IsLeftAnim)
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(16, 64, -16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                    else
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(48, 64, -16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                }
                else
                {
                    m_IsTurning = true;
                    m_Timer = m_TurnSpeed;
                    m_TileSprite.setTextureRect(sf::IntRect(16, 64, -16, 32));
                    m_MovementDirection = MovementDirection::Right;
                    m_IsMoving = true;

                }
            }
            m_IsMoving = true;
        }
        else if (yAmount != 0)
        {
            //Move up
            if (yAmount < 0)
            {
                if (m_MovementDirection == MovementDirection::Up && static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->tileIsWalkable(xPos / 32.f, yPos / 32.f + 1 - 1))
                {
                    m_OldXPos = xPos;
                    m_OldYPos = yPos;
                    yPos -= 32.f;
                    m_Timer = m_MoveSpeed;
                    m_IsMoving = true;
                    if (m_IsLeftAnim)
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(32, 32, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                    else
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(48, 32, -16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                }
                else
                {
                    m_IsMoving = true;
                    m_IsTurning = true;
                    m_Timer = m_TurnSpeed;
                    m_TileSprite.setTextureRect(sf::IntRect(32, 32, 16, 32));
                    m_MovementDirection = MovementDirection::Up;

                }

            }
            //Move down
            else
            {

                if (m_MovementDirection == MovementDirection::Down && static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->tileIsWalkable(xPos / 32.f, yPos / 32.f + 1 + 1))
                {
                    m_OldXPos = xPos;
                    m_OldYPos = yPos;
                    yPos += 32.f;
                    m_Timer = m_MoveSpeed;
                    m_IsMoving = true;
                    if (m_IsLeftAnim)
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(32, 0, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                    else
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(0, 0, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                }
                else if(static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->tileIsWalkable(xPos / 32.f, yPos / 32.f + 1 + 1))
                {
                    m_MovementDirection = MovementDirection::Down;
                    m_Timer = m_TurnSpeed;
                    m_IsTurning = true;
                    m_IsMoving = true;
                    if (m_IsLeftAnim)
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(0, 0, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;
                    }
                    else
                    {
                        m_TileSprite.setTextureRect(sf::IntRect(32, 0, 16, 32));
                        m_IsLeftAnim == false ? m_IsLeftAnim = true : m_IsLeftAnim = false;

                    }
                }
                else
                {
                    m_TileSprite.setTextureRect(sf::IntRect(0, 0, 16, 32));
                }
            }
        }
    }

}

void Player::LoadPlayerSprite()
{
    if (m_TileTexture.loadFromFile(m_TexturePath))
    {
        m_TileSprite.setTexture(m_TileTexture);
        m_TileSprite.setScale(2.f, 2.f);
        m_TileSprite.setPosition(xPos, yPos);
        m_TileSprite.setTextureRect(sf::IntRect(32, 64, -16, 32));
    }
}

void Player::Update(const float a_DeltaTime)
{
    if (m_IsMoving && !m_IsTurning)
    {
        m_TempXPos = xPos + (m_OldXPos - xPos) * (m_Timer / m_MoveSpeed);
        m_TempYPos = yPos + (m_OldYPos - yPos) * (m_Timer / m_MoveSpeed);
        m_TileSprite.setPosition(m_TempXPos, m_TempYPos);
        m_Timer -= a_DeltaTime;
        if (!m_hasAnimated && m_Timer < 0.12f)
        {
            if (m_MovementDirection == MovementDirection::Right)
            {
                m_TileSprite.setTextureRect(sf::IntRect(32, 64, -16, 32));
            }
            if (m_MovementDirection == MovementDirection::Left)
            {
                m_TileSprite.setTextureRect(sf::IntRect(16, 64, 16, 32));
            }
            if (m_MovementDirection == MovementDirection::Up)
            {
                m_TileSprite.setTextureRect(sf::IntRect(16, 32, 16, 32));
            }
            if (m_MovementDirection == MovementDirection::Down)
            {
                m_TileSprite.setTextureRect(sf::IntRect(16, 0, 16, 32));
            }
        }
        if (m_Timer < 0.f)
        {
            m_Timer = 0.f;
            m_IsMoving = false;
            m_TileSprite.setPosition(xPos, yPos);
            m_TempXPos = xPos;
            m_TempYPos = yPos;
            m_hasAnimated = false;
            if (xMoveAmount > 1 || yMoveAmount >> 1)
            {
                if (xMoveAmount > 1)
                {
                    MovePlayer(xMoveAmount - 1, yMoveAmount);
                }
                else if (xMoveAmount < -1)
                {
                    MovePlayer(xMoveAmount + 1, yMoveAmount);

                }
                else if (yMoveAmount > 1)
                {
                    MovePlayer(xMoveAmount, yMoveAmount - 1);
                }
                else if(yMoveAmount < -1)
                {
                    MovePlayer(xMoveAmount, yMoveAmount + 1);
                }
            }
            else
            {
                if (static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->m_IsRunningScript == true)
                {
                    static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->RunNextLine();
                }
                else if (static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->hasEventOnTile(m_TempXPos / 32.f, m_TempYPos / 32.f + 1))
                {
                    static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->runEventOnTile(m_TempXPos / 32.f, m_TempYPos / 32.f + 1);
                }
            }
            
        }
    }
    else if (m_IsMoving && m_IsTurning)
    {
        m_Timer -= a_DeltaTime;
        if (m_Timer < 0.f)
        {
            m_Timer = 0.f;
            m_IsMoving = false;
            m_IsTurning = false;            
            if (static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->m_IsRunningScript == true)
            {
                static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager).at(0))->RunNextLine();
            }
        }
    }
    m_PokemonHolder.UpdatePokemon(a_DeltaTime);
}

void Player::LoadAndDrawPokemon()
{
    m_PokemonHolder.LoadAndDrawPartyPokemon();
}

sf::Vector2f Player::getPos()
{
    return sf::Vector2f(xPos, yPos);
}

sf::Vector2f Player::getTempPos()
{
    return sf::Vector2f(m_TempXPos, m_TempYPos);
}
