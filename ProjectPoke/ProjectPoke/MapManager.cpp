#include "MapManager.h"
#include <assert.h>
#include "TileTypeEnum.h"
#include "ScriptEventManager.h"
#include "ManagerManager.h"
#include "Entitymanager.h"
MapManager::MapManager()
{
}

MapManager::~MapManager()
{
}

e_ManagerType MapManager::getManagerType()
{
    return e_ManagerType::MapManager;
}

void MapManager::Initialize()
{
    m_CurTileMap = m_MapLoader->LoadMap("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Maps/Map01.json");
    if (m_CurTileMap == 0)
    {
        printf("Map could not be properly read \r\n");
    }
    m_CurTileMap->loadTileSprites();
}

void MapManager::DrawMap(sf::RenderWindow& a_Window)
{
    m_CurTileMap->DrawTileMaps(a_Window);
}

bool MapManager::tileIsWalkable(int xPos, int yPos)
{
    for (Layer* l : m_CurTileMap->m_Layers)
    {
        if (yPos > l->m_TileMap.size() || xPos > l->m_TileMap[0].size())
        {
            return false;
        }
        else
        {
            if (l->m_TileMap[yPos][xPos]->m_Type == (int)TileType::notWalkable)
            {
                return false;
            }
        }
    }
    return true;
}

bool MapManager::hasEventOnTile(int xPos, int yPos)
{
    for (Layer* l : m_CurTileMap->m_Layers)
    {
        if (l->m_TileMap[yPos][xPos]->m_EventScript != "")
        {
            return true;
        }
    }
    return false;
}

bool MapManager::hasNpcAtDirection(MovementDirection a_Direction)
{
    int xPos = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.getPos().x / 32;
    int yPos = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.getPos().y / 32 + 1;

    if (a_Direction == MovementDirection::Left)
    {
        xPos -= 1;
    }
    else if (a_Direction == MovementDirection::Right)
    {
        xPos += 1;
    }
    else if (a_Direction == MovementDirection::Up)
    {
        yPos -= 1;
    }
    else if (a_Direction == MovementDirection::Down)
    {
        yPos += 1;
    }

    for (Layer* l : m_CurTileMap->m_Layers)
    {
        if (l->m_TileMap[yPos][xPos]->m_OccupyingNpcID != 0)
        {
            return true;
        }
    }
    return false;
}

void MapManager::runNpcScriptAtDirection(MovementDirection a_Direction)
{
    int xPos = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.getPos().x / 32;
    int yPos = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->player.getPos().y / 32 + 1;

    if (a_Direction == MovementDirection::Left)
    {
        xPos -= 1;
    }
    else if (a_Direction == MovementDirection::Right)
    {
        xPos += 1;
    }
    else if (a_Direction == MovementDirection::Up)
    {
        yPos -= 1;
    }
    else if (a_Direction == MovementDirection::Down)
    {
        yPos += 1;
    }
    for (Layer* l : m_CurTileMap->m_Layers)
    {
        if (l->m_TileMap[yPos][xPos]->m_OccupyingNpcID != 0)
        {
            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->RunNpcScriptByID(l->m_TileMap[yPos][xPos]->m_OccupyingNpcID);
            return;
        }
    }
}

void MapManager::runEventOnTile(int xPos, int yPos)
{
    for (Layer* l : m_CurTileMap->m_Layers)
    {
        if (l->m_TileMap[yPos][xPos]->m_EventScript != "")
        {
            dynamic_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager)[0])->StartScript(l->m_TileMap[yPos][xPos]->m_EventScript);
        }
    }
}

