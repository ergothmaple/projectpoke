#pragma once
#include "Pokemon.h"
#include <vector>
class PokedexData
{
public:
	PokedexData();
	~PokedexData();

private:
	std::vector<Pokemon*> m_DexPokemon;
};