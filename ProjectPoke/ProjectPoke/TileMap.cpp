#include "TileMap.h"
#include "DrawTileMap.h"
TileMap::TileMap()
{
}

TileMap::~TileMap()
{
}

void TileMap::ResizeTileMap()
{
    for (int i = 0; i < m_LayerAmount; i++)
    {
        Layer* l = new Layer();
        l->m_TileMap.resize(m_Height);
        for (int y = 0; y < m_Height; y++)
        {
            l->m_TileMap[y].resize(m_Width);
        }
        m_Layers.push_back(l);
    }



}

void TileMap::loadTileSprites()
{
    for (Layer* l : m_Layers)
    {
        l->LoadDrawTileMaps(m_Width, m_Height);
    }

}

void TileMap::DrawTileMaps(sf::RenderWindow& a_Window)
{
    for (Layer* l : m_Layers)
    {
        for (DrawTileMap& tm : l->drawTileMaps)
        {
            a_Window.draw(tm);
        }
    }
}

