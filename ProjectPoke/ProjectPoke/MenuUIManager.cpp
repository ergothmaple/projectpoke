#include "MenuUIManager.h"
#include "ManagerManager.h"
#include "EntityManager.h"
MenuUIManager::MenuUIManager()
{
}

MenuUIManager::~MenuUIManager()
{
}

e_ManagerType MenuUIManager::getManagerType()
{
    return e_ManagerType::MenuUIManager;
}

void MenuUIManager::Initialize()
{
    //Create anchors

    //Container anchors
    m_PokemonContainerAnchors[0] = sf::Vector2f(0.f, 0.f);
    m_PokemonContainerAnchors[1] = sf::Vector2f(128.f, 8.f);
    m_PokemonContainerAnchors[2] = sf::Vector2f(0.f, 48.f);
    m_PokemonContainerAnchors[3] = sf::Vector2f(128.f, 56.f);
    m_PokemonContainerAnchors[4] = sf::Vector2f(0.f, 96.f);
    m_PokemonContainerAnchors[5] = sf::Vector2f(128.f, 104.f);

    //Cancel button anchor
    m_PokemonContainerAnchors[6] = sf::Vector2f(200.f, 164.f);

    //Pokemon icon anchors
    m_PokemonIconAnchors[0] = sf::Vector2f(10.f, 5.f);
    m_PokemonIconAnchors[1] = sf::Vector2f(145.f, 12.f);
    m_PokemonIconAnchors[2] = sf::Vector2f(10.f, 53.f);
    m_PokemonIconAnchors[3] = sf::Vector2f(138.f, 61.f);
    m_PokemonIconAnchors[4] = sf::Vector2f(10.f, 101.f);
    m_PokemonIconAnchors[5] = sf::Vector2f(138.f, 109.f);
    //Pokemon level anchors
    m_PokemonLevelAnchors[0] = sf::Vector2f(21.f, 35.f);
    m_PokemonLevelAnchors[1] = sf::Vector2f(149.f, 43.f);
    m_PokemonLevelAnchors[2] = sf::Vector2f(21.f, 83.f);
    m_PokemonLevelAnchors[3] = sf::Vector2f(149.f, 91.f);
    m_PokemonLevelAnchors[4] = sf::Vector2f(21.f, 131.f);
    m_PokemonLevelAnchors[5] = sf::Vector2f(149.f, 139.f);

    //Pokemon CurHP anchors
    m_PokemonCurHPAnchors[0] = sf::Vector2f(60.f, 35.f);
    m_PokemonCurHPAnchors[1] = sf::Vector2f(188.f, 43.f);
    m_PokemonCurHPAnchors[2] = sf::Vector2f(60.f, 83.f);
    m_PokemonCurHPAnchors[3] = sf::Vector2f(188.f, 91.f);
    m_PokemonCurHPAnchors[4] = sf::Vector2f(60.f, 131.f);
    m_PokemonCurHPAnchors[5] = sf::Vector2f(188.f, 139.f);

    //Pokemon MaxHP anchors
    m_PokemonMaxHPAnchors[0] = sf::Vector2f(93.f, 35.f);
    m_PokemonMaxHPAnchors[1] = sf::Vector2f(221.f, 43.f);
    m_PokemonMaxHPAnchors[2] = sf::Vector2f(93.f, 83.f);
    m_PokemonMaxHPAnchors[3] = sf::Vector2f(221.f, 91.f);
    m_PokemonMaxHPAnchors[4] = sf::Vector2f(93.f, 131.f);
    m_PokemonMaxHPAnchors[5] = sf::Vector2f(221.f, 139.f);

    //Pokemon Name anchors
    m_PokemonNameAnchors[0] = sf::Vector2f(50.f, 9.f);
    m_PokemonNameAnchors[1] = sf::Vector2f(178.f, 17.f);
    m_PokemonNameAnchors[2] = sf::Vector2f(50.f, 57.f);
    m_PokemonNameAnchors[3] = sf::Vector2f(178.f, 65.f);
    m_PokemonNameAnchors[4] = sf::Vector2f(50.f, 105.f);
    m_PokemonNameAnchors[5] = sf::Vector2f(178.f, 113.f);

    //Pokemon Gender anchors
    m_PokemonGenderAnchors[0] = sf::Vector2f(110.f, 11.f);
    m_PokemonGenderAnchors[1] = sf::Vector2f(238.f, 19.f);
    m_PokemonGenderAnchors[2] = sf::Vector2f(110.f, 59.f);
    m_PokemonGenderAnchors[3] = sf::Vector2f(238.f, 67.f);
    m_PokemonGenderAnchors[4] = sf::Vector2f(110.f, 107.f);
    m_PokemonGenderAnchors[5] = sf::Vector2f(238.f, 115.f);

    //Pokemon Health anchors
    m_PokemonHealthBarAnchors[0] = sf::Vector2f(64.f, 25.f);
    m_PokemonHealthBarAnchors[1] = sf::Vector2f(192.f, 33.f);
    m_PokemonHealthBarAnchors[2] = sf::Vector2f(64.f, 73.f);
    m_PokemonHealthBarAnchors[3] = sf::Vector2f(192.f, 81.f);
    m_PokemonHealthBarAnchors[4] = sf::Vector2f(64.f, 121.f);
    m_PokemonHealthBarAnchors[5] = sf::Vector2f(192.f, 129.f);

    if (!m_Font.loadFromFile("C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Fonts/Pokemon.ttf"))
    {
        printf("Can't load font \r\n");
    }

    CreateMenuNumberDataHolders();
    UpdateMenu();
}

void MenuUIManager::DrawPartyMenu(sf::RenderWindow& a_Window, sf::View& a_Camera)
{
    if (b_MenuShouldBeDrawn)
    {
        //Update menu transform
        m_MenuTransform.setPosition(a_Camera.getCenter() - sf::Vector2f(a_Window.getSize().x / 2.f, a_Window.getSize().y / 2.f));
        //Draw the menu
        a_Window.draw(*this);

        //Get all party pokemon
        std::vector<Pokemon*> partyPokemon = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_PokemonHolder.GetPartyPokemon();

        //Draw icon of each pokemon in the party
        int index = 0;
        for (Pokemon* pokemon : partyPokemon)
        {
            pokemon->DrawIcon(m_MenuTransform.getPosition() + m_PokemonIconAnchors[index] * 2.f, a_Window);
            index++;
        }
        for (int i = 0; i < m_PokemonNames.size(); i++)
        {
            m_PokemonNames.at(i).setPosition(m_MenuTransform.getPosition() + m_PokemonNameAnchors[i] * 2.f);
            a_Window.draw(m_PokemonNames.at(i));
        }
    }
}

void MenuUIManager::InitializeVertexArray()
{
    if (m_PartyMenuTexture.loadFromFile(m_PartyMenuTexturePath))
    {
        m_PartyMenuVertices.setPrimitiveType(sf::Quads);
        //2 objects for now
        m_PartyMenuVertices.resize(m_MenuObjects.size() * 4);
        for (int i = 0; i < m_MenuObjects.size(); i++)
        {
            const MenuObjectData menuObject = m_MenuObjects[i];
            sf::Vertex* quad = &m_PartyMenuVertices[i * 4];
            quad[0].position = sf::Vector2f(menuObject.m_LeftUpPosition.x * menuObject.m_Scale.x, menuObject.m_LeftUpPosition.y * menuObject.m_Scale.y);
            quad[1].position = sf::Vector2f(menuObject.m_RightDownPosition.x * menuObject.m_Scale.x, menuObject.m_LeftUpPosition.y * menuObject.m_Scale.y);
            quad[2].position = sf::Vector2f(menuObject.m_RightDownPosition.x * menuObject.m_Scale.x, menuObject.m_RightDownPosition.y * menuObject.m_Scale.y);
            quad[3].position = sf::Vector2f(menuObject.m_LeftUpPosition.x * menuObject.m_Scale.x, menuObject.m_RightDownPosition.y * menuObject.m_Scale.y);

            // define its 4 texture coordinates
            quad[0].texCoords = sf::Vector2f(menuObject.m_LeftUpTexCoord.x, menuObject.m_LeftUpTexCoord.y);
            quad[1].texCoords = sf::Vector2f(menuObject.m_RightDownTexCoord.x, menuObject.m_LeftUpTexCoord.y);
            quad[2].texCoords = sf::Vector2f(menuObject.m_RightDownTexCoord.x, menuObject.m_RightDownTexCoord.y);
            quad[3].texCoords = sf::Vector2f(menuObject.m_LeftUpTexCoord.x, menuObject.m_RightDownTexCoord.y);
        }
    }
}

void MenuUIManager::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform = m_MenuTransform.getTransform();
    states.texture = &m_PartyMenuTexture;
    target.draw(m_PartyMenuVertices, states);
}

void MenuUIManager::UpdateMenu()
{
    m_MenuObjects.clear();
    m_PokemonNames.clear();
    //Menu Background
    MenuObjectData m_PartyMenuBackground;
    m_PartyMenuBackground.m_Scale = sf::Vector2f(2.f, 2.f);
    m_PartyMenuBackground.m_LeftUpPosition = sf::Vector2f(0.f, 0.f);
    m_PartyMenuBackground.m_RightDownPosition = sf::Vector2f(256.f, 192.f);
    m_PartyMenuBackground.m_LeftUpTexCoord = sf::Vector2f(0.f, 0.f);
    m_PartyMenuBackground.m_RightDownTexCoord = sf::Vector2f(256.f, 192.f);
    m_MenuObjects.push_back(m_PartyMenuBackground);



    //Create pokemon containers
    std::vector<Pokemon*> partyPokemon = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_PokemonHolder.GetPartyPokemon();

    //Create container for each pokemon
    for (int i = 0; i < partyPokemon.size(); i++)
    {
        //Only create container if the anchor exists
        if (m_PokemonContainerAnchors.find(i) != m_PokemonContainerAnchors.end())
        {
            //Create Container
            MenuObjectData m_PartyMenuPokemon1Container;

            //Fill container
            //First container is differrent from the others
            if (i == 0)
            {
                m_PartyMenuPokemon1Container.m_Scale = sf::Vector2f(2.f, 2.f);
                m_PartyMenuPokemon1Container.m_LeftUpPosition = m_PokemonContainerAnchors.find(i)->second;
                m_PartyMenuPokemon1Container.m_RightDownPosition = m_PokemonContainerAnchors.find(i)->second + sf::Vector2f(128.f, 49.f);
                //Display highlighted box if selected
                if (SelectedIndex == i)
                {
                    m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(384.f, 50.f);
                    m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(512.f, 99.f);
                }
                else
                {
                    if (partyPokemon.at(i)->m_CurHP > 0)
                    {
                        m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(256.f, 50.f);
                        m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(384.f, 99.f);
                    }
                    else
                    {
                        m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(512.f, 50.f);
                        m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(640.f, 99.f);
                    }
                }
            }
            else
            {
                m_PartyMenuPokemon1Container.m_Scale = sf::Vector2f(2.f, 2.f);
                m_PartyMenuPokemon1Container.m_LeftUpPosition = m_PokemonContainerAnchors.find(i)->second;
                m_PartyMenuPokemon1Container.m_RightDownPosition = m_PokemonContainerAnchors.find(i)->second + sf::Vector2f(128.f, 49.f);

                //Display highlighted box if selected
                if (SelectedIndex == i)
                {
                    m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(384.f, 0.f);
                    m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(512.f, 49.f);
                }
                else
                {
                    if (partyPokemon.at(i)->m_CurHP > 0)
                    {
                        m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(256.f, 0.f);
                        m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(384.f, 49.f);
                    }
                    else
                    {
                        m_PartyMenuPokemon1Container.m_LeftUpTexCoord = sf::Vector2f(512.f, 0.f);
                        m_PartyMenuPokemon1Container.m_RightDownTexCoord = sf::Vector2f(640.f, 49.f);

                    }
                }
            }

            //Add container to the vector
            m_MenuObjects.push_back(m_PartyMenuPokemon1Container);
        }

        //Update Level text
        int pokemonLevel = partyPokemon.at(i)->m_Level;
        sf::Vector2f m_LevelTextOffset = sf::Vector2f(0.f, 0.f);
        if (pokemonLevel / 100 > 0)
        {
            int remainderLevel = 1;
            MenuObjectData m_Level = m_MenuNumbers.at(remainderLevel);
            m_Level.m_LeftUpPosition = m_PokemonLevelAnchors[i] + m_LevelTextOffset;
            m_Level.m_RightDownPosition = m_PokemonLevelAnchors[i] + m_Level.m_RightDownTexCoord - m_Level.m_LeftUpTexCoord + m_LevelTextOffset;
            m_Level.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_Level);
            m_LevelTextOffset = m_LevelTextOffset + sf::Vector2f(8.f, 0.f);
        }
        if (pokemonLevel / 10 > 0)
        {
            int remainderLevel = (pokemonLevel % 100) / 10;
            MenuObjectData m_Level = m_MenuNumbers.at(remainderLevel);
            m_Level.m_LeftUpPosition = m_PokemonLevelAnchors[i] + m_LevelTextOffset;
            m_Level.m_RightDownPosition = m_PokemonLevelAnchors[i] + m_Level.m_RightDownTexCoord - m_Level.m_LeftUpTexCoord + m_LevelTextOffset;
            m_Level.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_Level);
            m_LevelTextOffset = m_LevelTextOffset + sf::Vector2f(8.f, 0.f);

        }
        int remainderLevel = pokemonLevel % 10;
        MenuObjectData m_Level = m_MenuNumbers.at(remainderLevel);
        m_Level.m_LeftUpPosition = m_PokemonLevelAnchors[i] + m_LevelTextOffset;
        m_Level.m_RightDownPosition = m_PokemonLevelAnchors[i] + m_Level.m_RightDownTexCoord - m_Level.m_LeftUpTexCoord + m_LevelTextOffset;
        m_Level.m_Scale = sf::Vector2f(2.f, 2.f);
        m_MenuObjects.push_back(m_Level);


        //Update Cur HP text
        int curHP = partyPokemon.at(i)->m_CurHP;
        sf::Vector2f m_CurHPOffset = sf::Vector2f(16.f, 0.f);
        int remainderCurHP = curHP % 10;
        MenuObjectData m_CurHP = m_MenuNumbers.at(remainderCurHP);
        m_CurHP.m_LeftUpPosition = m_PokemonCurHPAnchors[i] + m_CurHPOffset;
        m_CurHP.m_RightDownPosition = m_PokemonCurHPAnchors[i] + m_CurHP.m_RightDownTexCoord - m_CurHP.m_LeftUpTexCoord + m_CurHPOffset;
        m_CurHPOffset = m_CurHPOffset - sf::Vector2f(8.f, 0.f);
        m_CurHP.m_Scale = sf::Vector2f(2.f, 2.f);
        m_MenuObjects.push_back(m_CurHP);

        if (curHP / 10 > 0)
        {
            int remainderCurHP = (curHP % 100) / 10;
            MenuObjectData m_CurHP = m_MenuNumbers.at(remainderCurHP);
            m_CurHP.m_LeftUpPosition = m_PokemonCurHPAnchors[i] + m_CurHPOffset;
            m_CurHP.m_RightDownPosition = m_PokemonCurHPAnchors[i] + m_CurHP.m_RightDownTexCoord - m_CurHP.m_LeftUpTexCoord + m_CurHPOffset;
            m_CurHPOffset = m_CurHPOffset - sf::Vector2f(8.f, 0.f);
            m_CurHP.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_CurHP);
        }
        if (curHP / 100 > 0)
        {
            int remainderCurHP = (curHP % 1000) / 100;
            MenuObjectData m_CurHP = m_MenuNumbers.at(remainderCurHP);
            m_CurHP.m_LeftUpPosition = m_PokemonCurHPAnchors[i] + m_CurHPOffset;
            m_CurHP.m_RightDownPosition = m_PokemonCurHPAnchors[i] + m_CurHP.m_RightDownTexCoord - m_CurHP.m_LeftUpTexCoord + m_CurHPOffset;
            m_CurHPOffset = m_CurHPOffset - sf::Vector2f(8.f, 0.f);
            m_CurHP.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_CurHP);
        }

        //Update Max HP text
        int MaxHP = partyPokemon.at(i)->m_MaxHP;
        sf::Vector2f m_MaxHPOffset = sf::Vector2f(0.f, 0.f);
        if (MaxHP / 100 > 0)
        {
            int remainderMaxHP = (MaxHP % 1000) / 100;
            MenuObjectData m_MaxHP = m_MenuNumbers.at(remainderMaxHP);
            m_MaxHP.m_LeftUpPosition = m_PokemonMaxHPAnchors[i] + m_MaxHPOffset;
            m_MaxHP.m_RightDownPosition = m_PokemonMaxHPAnchors[i] + m_MaxHP.m_RightDownTexCoord - m_MaxHP.m_LeftUpTexCoord + m_MaxHPOffset;
            m_MaxHPOffset = m_MaxHPOffset + sf::Vector2f(8.f, 0.f);
            m_MaxHP.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_MaxHP);
        }
        if (MaxHP / 10 > 0)
        {
            int remainderMaxHP = (MaxHP % 100) / 10;
            MenuObjectData m_MaxHP = m_MenuNumbers.at(remainderMaxHP);
            m_MaxHP.m_LeftUpPosition = m_PokemonMaxHPAnchors[i] + m_MaxHPOffset;
            m_MaxHP.m_RightDownPosition = m_PokemonMaxHPAnchors[i] + m_MaxHP.m_RightDownTexCoord - m_MaxHP.m_LeftUpTexCoord + m_MaxHPOffset;
            m_MaxHPOffset = m_MaxHPOffset + sf::Vector2f(8.f, 0.f);
            m_MaxHP.m_Scale = sf::Vector2f(2.f, 2.f);
            m_MenuObjects.push_back(m_MaxHP);
        }
        int remainderMaxHP = (MaxHP % 10);
        MenuObjectData m_MaxHP = m_MenuNumbers.at(remainderMaxHP);
        m_MaxHP.m_LeftUpPosition = m_PokemonMaxHPAnchors[i] + m_MaxHPOffset;
        m_MaxHP.m_RightDownPosition = m_PokemonMaxHPAnchors[i] + m_MaxHP.m_RightDownTexCoord - m_MaxHP.m_LeftUpTexCoord + m_MaxHPOffset;
        m_MaxHP.m_Scale = sf::Vector2f(2.f, 2.f);
        m_MenuObjects.push_back(m_MaxHP);

        //Update pokemon text
        sf::Text pokemonName;
        pokemonName.setFont(m_Font);
        pokemonName.setFillColor(sf::Color::Black);
        pokemonName.setCharacterSize(25);
        pokemonName.setLineSpacing(0.8f);
        if (partyPokemon.at(i)->m_NickName == "")
        {
            pokemonName.setString(partyPokemon.at(i)->m_Name);
        }
        else
        {
            pokemonName.setString(partyPokemon.at(i)->m_NickName);
        }
        pokemonName.setPosition(m_MenuTransform.getPosition() + m_PokemonNameAnchors[i]);
        m_PokemonNames.push_back(pokemonName);


        //Create gender icons
        MenuObjectData m_Gender;
        m_Gender.m_Scale = sf::Vector2f(2.f, 2.f);
        m_Gender.m_LeftUpPosition = m_PokemonGenderAnchors[i];
        m_Gender.m_RightDownPosition = m_PokemonGenderAnchors[i];
        m_Gender.m_LeftUpTexCoord = sf::Vector2f(265, 133) + sf::Vector2f(8.f, 0.f) * (float)partyPokemon.at(i)->m_Gender;
        m_Gender.m_RightDownTexCoord = sf::Vector2f(273, 144) + sf::Vector2f(8.f, 0.f) * (float)partyPokemon.at(i)->m_Gender;
        m_Gender.m_RightDownPosition = m_PokemonGenderAnchors[i] + m_Gender.m_RightDownTexCoord - m_Gender.m_LeftUpTexCoord;
        m_MenuObjects.push_back(m_Gender);

        //Update healthbar
        float healthPercentage = (float)partyPokemon.at(i)->m_CurHP / (float)partyPokemon.at(i)->m_MaxHP;
        MenuObjectData m_HealthBar;
        m_HealthBar.m_Scale = sf::Vector2f(2.f, 2.f);
        m_HealthBar.m_LeftUpPosition = m_PokemonHealthBarAnchors[i];
        m_HealthBar.m_RightDownPosition = m_PokemonHealthBarAnchors[i] + sf::Vector2f(49.f * healthPercentage, 6);
        if (healthPercentage < 0.25f)
        {
            m_HealthBar.m_LeftUpTexCoord = sf::Vector2f(283, 133);
            m_HealthBar.m_RightDownTexCoord = sf::Vector2f(295, 139);
        }
        else if (healthPercentage < 0.5f)
        {
            m_HealthBar.m_LeftUpTexCoord = sf::Vector2f(283, 140);
            m_HealthBar.m_RightDownTexCoord = sf::Vector2f(295, 146);
        }
        else
        {
            m_HealthBar.m_LeftUpTexCoord = sf::Vector2f(283, 147);
            m_HealthBar.m_RightDownTexCoord = sf::Vector2f(295, 153);
        }
        m_MenuObjects.push_back(m_HealthBar);

    }

    //Create cancel button container
    MenuObjectData m_PartyMenuCancelButton;
    m_PartyMenuCancelButton.m_Scale = sf::Vector2f(2.f, 2.f);
    m_PartyMenuCancelButton.m_LeftUpPosition = m_PokemonContainerAnchors.find(6)->second;
    m_PartyMenuCancelButton.m_RightDownPosition = m_PokemonContainerAnchors.find(6)->second + sf::Vector2f(55.f, 23.f);
    if (SelectedIndex == 6 || SelectedIndex == 7)
    {
        m_PartyMenuCancelButton.m_LeftUpTexCoord = sf::Vector2f(314.f, 108.f);
        m_PartyMenuCancelButton.m_RightDownTexCoord = sf::Vector2f(370.f, 132.f);
    }
    else
    {
        m_PartyMenuCancelButton.m_LeftUpTexCoord = sf::Vector2f(257.f, 108.f);
        m_PartyMenuCancelButton.m_RightDownTexCoord = sf::Vector2f(313.f, 132.f);
    }
    m_MenuObjects.push_back(m_PartyMenuCancelButton);

    

    //Update vertex array
    InitializeVertexArray();
}

void MenuUIManager::UpdateSelectIndex(Direction a_Direction)
{
    int partyPokemonSize = static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->player.m_PokemonHolder.GetPartyPokemon().size();

    switch (a_Direction)
    {
    case Direction::None:
        SelectedIndex = 0;
        break;
    case Direction::Up:
        if (SelectedIndex == 0)
        {
            SelectedIndex = 6;
        }
        else if (SelectedIndex == 1)
        {
            SelectedIndex = 7;
        }
        else if (SelectedIndex == 6)
        {
            int remainder = partyPokemonSize % 2;
            SelectedIndex = partyPokemonSize - 2 + remainder;
        }
        else if (SelectedIndex == 7)
        {

            int remainder = partyPokemonSize % 2;
            SelectedIndex = partyPokemonSize - 1 - remainder;
        }
        else
        {
            SelectedIndex = SelectedIndex - 2;
        }
        break;
    case Direction::Down:
        if (SelectedIndex + 2 < partyPokemonSize)
        {
            SelectedIndex += 2;
        }
        else if (SelectedIndex % 2 == 0 && SelectedIndex != 6)
        {
            SelectedIndex = 6;
        }
        else if (SelectedIndex % 2 == 1 && SelectedIndex != 7)
        {
            SelectedIndex = 7;
        }
        else
        {
            SelectedIndex -= 6;
        }
        break;
    case Direction::Left:
        if (SelectedIndex == 0)
        {
            SelectedIndex = 6;
        }
        else if (SelectedIndex == 6 || SelectedIndex == 7)
        {
            SelectedIndex = partyPokemonSize - 1;
        }
        else
        {
            SelectedIndex--;
        }
        break;
    case Direction::Right:
        if (SelectedIndex + 1 == partyPokemonSize)
        {
            int remainder = partyPokemonSize % 2;
            SelectedIndex = 6 + remainder;
        }
        else if (SelectedIndex == 6 || SelectedIndex == 7)
        {
            SelectedIndex = 0;
        }
        else
        {
            SelectedIndex++;
        }
        break;
    default:
        break;
    }
    UpdateMenu();
}

void MenuUIManager::CreateMenuNumberDataHolders()
{
    for (int i = 0; i < 10; i++)
    {
        MenuObjectData menuObject;
        menuObject.m_LeftUpPosition = sf::Vector2f(0.f, 0.f);
        menuObject.m_RightDownPosition = sf::Vector2f(0.f, 0.f);
        menuObject.m_Scale = sf::Vector2f(1.f, 1.f);
        menuObject.m_LeftUpTexCoord = sf::Vector2f(257 + 8 * i, 99);
        menuObject.m_RightDownTexCoord = sf::Vector2f(257 + 8 * i + 8, 106);
        m_MenuNumbers.push_back(menuObject);
    }
}
