#pragma once
#include "ManagerEnum.h"
class BaseManager
{
public:
	BaseManager();
	~BaseManager();
	virtual e_ManagerType getManagerType();
	virtual void Initialize();
protected:
private:
};