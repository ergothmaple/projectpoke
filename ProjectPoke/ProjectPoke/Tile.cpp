#include "Tile.h"
#include <SFML/Graphics/Rect.hpp>
Tile::Tile()
{
}

Tile::~Tile()
{
}

void Tile::LoadTileSprite()
{
    if (m_TileTexture.loadFromFile(m_TexturePath))
    {
        m_TileSprite.setTexture(m_TileTexture);
        m_TileSprite.setTextureRect(sf::IntRect(m_TextureXY.x  * 32, m_TextureXY.y * 32, m_TextureWidthHeight.x, m_TextureWidthHeight.y));
    }
}
