#pragma once
#include <map>
#include <iostream>
#include <functional>
#include <string>
#include <vector>




class ScriptEventReader
{
public:
	ScriptEventReader();
	~ScriptEventReader();
    void Initialize();
	bool ReadFile(std::string m_FilePath);
	void ProcessNextLine();
	void RunFunc(std::string a_FuncName, std::vector<std::string> a_Args);


	static void printAStringAndTwoNumbers(std::string str1, int num1, int num2);
	static void printTwoNumbers(int num1, int num2);

public:
	bool m_IsProcessingFile = false;
private:
	typedef void (*randomTemplateType)();
	std::map<std::string, randomTemplateType> functionContainer;
	std::vector<std::string> fileLines;
};
