#include "Layer.h"
#include <iostream>
#include <map>
Layer::Layer()
{
}

Layer::~Layer()
{
}

void Layer::LoadDrawTileMaps(int width, int height)
{
    int index = 0;
    std::vector<std::vector<Tile*>> tempTileDrawLoader;
    for (int y = 0; y < m_TileMap.size(); y++)
    {
        for (int x = 0; x < m_TileMap[0].size(); x++)
        {
            if (stringTileMap.find(m_TileMap[y][x]->m_TexturePath) != stringTileMap.end())
            {
                    tempTileDrawLoader[stringTileMap.find(m_TileMap[y][x]->m_TexturePath)->second].push_back(m_TileMap[y][x]);
            }
            else
            {
                stringTileMap[m_TileMap[y][x]->m_TexturePath] = index;
                DrawTileMap drawTilemap;
                drawTilemap.m_TexturePath = m_TileMap[y][x]->m_TexturePath;
                drawTileMaps.push_back(drawTilemap);
                std::vector<Tile*> tileVec;
                tempTileDrawLoader.push_back(tileVec);
                tempTileDrawLoader[stringTileMap.find(m_TileMap[y][x]->m_TexturePath)->second].push_back(m_TileMap[y][x]);
                index++;
            }
        }
    }
    index = 0;
    drawTileMaps;
    for (DrawTileMap drawTileMap : drawTileMaps)
    {
        drawTileMaps[index].load(drawTileMaps[index].m_TexturePath, sf::Vector2u(32, 32), tempTileDrawLoader[index], width, height );
        index++;
    }
    stringTileMap;
}
