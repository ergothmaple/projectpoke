#include "FunctionManager.h"
#include "ScriptableFunctions.h"
#include "ScriptableFunction.h"

/**
 * \brief Helper for managing dynamic functions.
 */
FunctionManager::FunctionManager()
{
    functionContainer["MovePlayerX"] = genericTemplate(ScriptableFunctions::MovePlayerX);
    functionContainer["MovePlayerY"] = genericTemplate(ScriptableFunctions::MovePlayerY);
    functionContainer["SayDialogue"] = genericTemplate(ScriptableFunctions::SayDialogue);
    functionContainer["SayOptionDialogue"] = genericTemplate(ScriptableFunctions::SayOptionDialogue);
    functionContainer["SetVariable"] = genericTemplate(ScriptableFunctions::SetVariable);
    functionContainer["GivePokemon"] = genericTemplate(ScriptableFunctions::GivePokemon);
}

/**
 * \brief Default deconstructor.
 */
FunctionManager::~FunctionManager() = default;

/**
 * \brief Dynamically run a function which exists in the functionContainer map by passing it's name & args.
 * \param functionName Name of the dynamic function to run (must exist in the functionContainer)
 * \param args List of arguments to pass into the function.
 */
void FunctionManager::Invoke(const std::string & functionName, std::vector<std::string> & args)
{
    //Check if the function name exits in the map
    if (functionContainer.find(functionName) == functionContainer.end())
    {
        return;
    }
    //Invoke the function
    reinterpret_cast<void(*)(std::vector<std::string>)>(functionContainer[functionName])(args);
}

/**
 * \brief Dynamically run a function which exists in the functionContainer map by passing it an actual ScriptableFunction object.
 * \param functionName ScriptableFunction to dynamically run.
 */
void FunctionManager::Invoke(const ScriptableFunction & scriptableFunction)
{
    if (&scriptableFunction == nullptr)
    {
        return;
    }
    if (functionContainer.find(scriptableFunction.FunctionName) == functionContainer.end())
    {
        return;
    }
    reinterpret_cast<void(*)(std::vector<std::string>)>(functionContainer[scriptableFunction.FunctionName])(scriptableFunction.Arguments);
}