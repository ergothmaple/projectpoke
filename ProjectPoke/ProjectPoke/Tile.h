#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include <iostream>
class Tile
{
public:
	Tile();
	~Tile();

	void LoadTileSprite();

	int m_Type; //Make enum of this later
	std::string m_TexturePath; //Make texture pooling later
	std::string m_EventScript;
	//Possible encounterable enemies
	std::string m_EncounterableEnemies;
	int m_OccupyingNpcID;
	sf::Sprite m_TileSprite;
	sf::Texture m_TileTexture;
	sf::Vector2<int> m_TextureXY;
	sf::Vector2<int> m_TextureWidthHeight;
	sf::Vector2<int> m_Position;
	bool m_HasTileUnder;
	bool m_ChangeElevation;

private:
};

