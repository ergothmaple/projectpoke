#pragma once
#include <vector>
#include "Tile.h"
#include "DrawTileMap.h"
class Layer
{
public:
	Layer();
	~Layer();
	std::vector<std::vector<Tile*>> m_TileMap;
	void LoadDrawTileMaps(int width, int height);
	std::vector<DrawTileMap> drawTileMaps;
private:
	//index, path
	std::map<std::string, int> stringTileMap;
};