#pragma once
enum class TileType
{
    Walkable,
    notWalkable,
    Surfable,
    Interactable
};