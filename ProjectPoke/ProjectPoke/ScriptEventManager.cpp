#include "ScriptEventManager.h"

ScriptEventManager::ScriptEventManager()
{
}

ScriptEventManager::~ScriptEventManager()
{
}

e_ManagerType ScriptEventManager::getManagerType()
{
    return e_ManagerType::ScriptEventManager;
}

void ScriptEventManager::Initialize()
{
    functionManager.variableMap["StarterPokemon"] = "Yeet";
    functionManager.variableMap["PlayerName"] = "Mr Pepeja";
}

void ScriptEventManager::StartScript(std::string a_ScriptName)
{
    if (!m_IsRunningScript)
    {
        // load our script objects from the file
        m_CurrentScriptName = a_ScriptName;
        auto lines = fileManager.GetFileLines(a_ScriptName);
        functionManager.functionObjects = scriptParser.Parse(lines, functionManager.variableMap);
        m_Index = 0;
        RunNextLine();
        m_IsRunningScript = true;
    }
    else
    {
        printf("Another script is already running with name: %s \r\n", m_CurrentScriptName.c_str());
    }
}

void ScriptEventManager::RestartScriptFromCurrentLine()
{
    functionManager.functionObjects.clear();
    auto lines = fileManager.GetFileLines(m_CurrentScriptName);
    m_Index++;

    //Remove lines that have already been ran from the list
    while (m_Index > 0)
    {
        lines.erase(lines.begin());
        m_Index--;
    }

    functionManager.functionObjects = scriptParser.Parse(lines, functionManager.variableMap);
    RunNextLine();
}

void ScriptEventManager::RunNextLine()
{

    if (m_Index < functionManager.functionObjects.size())
    {
        if (functionManager.functionObjects.at(m_Index)->FunctionName.find("{{+DNR+}}") != std::string::npos)
        {
            m_Index++;
            RunNextLine();
            return;
        }
        functionManager.Invoke(*functionManager.functionObjects.at(m_Index));
        m_Index++;
    }
    else
    {
        FinishScript();
    }
}

void ScriptEventManager::FinishScript()
{
    functionManager.functionObjects.clear();
    m_Index = 0;
    m_CurrentScriptName = "";
    m_IsRunningScript = false;
}

FunctionManager& ScriptEventManager::getFunctionManager()
{
    return functionManager;
}
