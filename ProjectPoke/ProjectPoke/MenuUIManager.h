#pragma once
#include "BaseManager.h"
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/Graphics/VertexArray.hpp"
#include <map>
#include "MenuObjectData.h"
#include "DirectionEnum.h"
class MenuUIManager : public BaseManager, public sf::Drawable, public sf::Transformable
{
public:
	MenuUIManager();
	~MenuUIManager();
	e_ManagerType getManagerType() override;
	void Initialize() override;
	void DrawPartyMenu(sf::RenderWindow& a_Window, sf::View& a_Camera);
	void InitializeVertexArray();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	void UpdateMenu();
	void UpdateSelectIndex(Direction a_Direction);
	void CreateMenuNumberDataHolders();
public:
	bool b_MenuShouldBeDrawn = false;
private:
	int SelectedIndex = 0;
	sf::Font m_Font;
	sf::Transformable m_MenuTransform;
	sf::Texture m_PartyMenuTexture;
	std::string m_PartyMenuTexturePath = "C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Sprites/UI/PartyMenu.png";
	sf::VertexArray m_PartyMenuVertices;
	std::map<int, sf::Vector2f> m_PokemonContainerAnchors;
	std::map<int, sf::Vector2f> m_PokemonLevelAnchors;
	std::map<int, sf::Vector2f> m_PokemonIconAnchors;
	std::map<int, sf::Vector2f> m_PokemonCurHPAnchors;
	std::map<int, sf::Vector2f> m_PokemonMaxHPAnchors;
	std::map<int, sf::Vector2f> m_PokemonNameAnchors;
	std::map<int, sf::Vector2f> m_PokemonGenderAnchors;
	std::map<int, sf::Vector2f> m_PokemonHealthBarAnchors;
	std::vector<MenuObjectData> m_MenuNumbers;
	std::vector<MenuObjectData> m_MenuObjects;
	std::vector<sf::Text> m_PokemonNames;
	
};