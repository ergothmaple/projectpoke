#include "MapLoader.h"
#include <fstream>
#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"
#include <rapidjson/istreamwrapper.h>
#include "ManagerManager.h"
#include "EntityManager.h"
MapLoader::MapLoader()
{
}

MapLoader::~MapLoader()
{
}

TileMap* MapLoader::LoadMap(std::basic_string<char> a_MapName)
{
    //Load and parse file
    std::ifstream myFile(a_MapName);
    rapidjson::IStreamWrapper WrappedFile(myFile);
    if (!myFile.is_open())
    {
        printf("[ERROR] Couldn't find map file with path %s \r\n", a_MapName.c_str());
        return 0;
    }
    rapidjson::Document doc;
    if (doc.ParseStream(WrappedFile).HasParseError())
    {
        printf("[ERROR] Parse error while parsing the map.\r\n");
        return 0;
    }


    TileMap* tMap = new TileMap;

    //Read and load in map data
    if (doc.HasMember("MapName"))
    {
        tMap->m_MapName = doc.FindMember("MapName")->value.GetString();
    }
    if (doc.HasMember("Height"))
    {
        tMap->m_Height = doc.FindMember("Height")->value.GetInt();
    }
    if (doc.HasMember("Width"))
    {
        tMap->m_Width = doc.FindMember("Width")->value.GetInt();
    }
    if (doc.HasMember("AudioPath"))
    {
        tMap->m_AudioPath = doc.FindMember("AudioPath")->value.GetString();
    }
    if (doc.HasMember("WeatherType"))
    {
        tMap->m_Weather = doc.FindMember("WeatherType")->value.GetInt();
    }
    if (doc.HasMember("IsInside"))
    {
        tMap->m_Inside = doc.FindMember("IsInside")->value.GetBool();
    }
    if (doc.HasMember("LayerAmount"))
    {
        tMap->m_LayerAmount = doc.FindMember("LayerAmount")->value.GetInt();
    }

    //Resize Tilemap to fit all Tiles
    tMap->ResizeTileMap();

    int index = 0;
    for (Layer* l : tMap->m_Layers)
    {

        //Read and fill in Tile data
        if (doc["Layers"][index].HasMember("Tiles"))
        {
            for (int y = 0; y < tMap->m_Height; y++)
            {
                for (int x = 0; x < tMap->m_Width; x++)
                {
                    if (doc["Layers"][index]["Tiles"][y][x].HasMember("Type"))
                    {
                        Tile* tile = new Tile;
                        tile->m_Type = doc["Layers"][index]["Tiles"][y][x].FindMember("Type")->value.GetInt();
                        tile->m_TexturePath = doc["Layers"][index]["Tiles"][y][x].FindMember("TexturePath")->value.GetString();
                        tile->m_TextureXY.x = doc["Layers"][index]["Tiles"][y][x].FindMember("TextureX")->value.GetInt();
                        tile->m_TextureXY.y = doc["Layers"][index]["Tiles"][y][x].FindMember("TextureY")->value.GetInt();
                        tile->m_Position.x = doc["Layers"][index]["Tiles"][y][x].FindMember("TileX")->value.GetInt();
                        tile->m_Position.y = doc["Layers"][index]["Tiles"][y][x].FindMember("TileY")->value.GetInt();
                        tile->m_TextureWidthHeight.x = doc["Layers"][index]["Tiles"][y][x].FindMember("TextureWidth")->value.GetInt();
                        tile->m_TextureWidthHeight.y = doc["Layers"][index]["Tiles"][y][x].FindMember("TextureHeight")->value.GetInt();
                        tile->m_EventScript = doc["Layers"][index]["Tiles"][y][x].FindMember("EventScript")->value.GetString();
                        tile->m_EncounterableEnemies = doc["Layers"][index]["Tiles"][y][x].FindMember("EncounterableEnemies")->value.GetString();
                        tile->m_OccupyingNpcID = doc["Layers"][index]["Tiles"][y][x].FindMember("OccupyingNPCID")->value.GetInt();
                        tile->m_HasTileUnder = doc["Layers"][index]["Tiles"][y][x].FindMember("hasTileUnder")->value.GetBool();
                        tile->m_ChangeElevation = doc["Layers"][index]["Tiles"][y][x].FindMember("changeElevation")->value.GetBool();

                        if (tile->m_OccupyingNpcID > 0)
                        {
                            static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager)[0])->AddNpcByID(tile->m_OccupyingNpcID, x, y);
                        }
                        tMap->m_Layers[index]->m_TileMap[y][x] = tile;
                    }
                }
            }
        }

        index++;
    }

    return tMap;
}
