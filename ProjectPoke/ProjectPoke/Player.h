	#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>
#include "MovementDirectionEnum.h"
#include "PlayerPokemonHolder.h"
#include "PokedexData.h"
class Player
{
public:
	Player();
	~Player();
	void MovePlayer(int xAmount, int yAmount);
	void LoadPlayerSprite();
	void Update(const float a_DeltaTime);
	void LoadAndDrawPokemon();
	sf::Sprite m_TileSprite;
	sf::Vector2f getPos();
	sf::Vector2f getTempPos();
	bool m_IsMoving = false;
	bool m_MovementEnabled = true;
	MovementDirection m_MovementDirection = MovementDirection::None;
	PlayerPokemonHolder m_PokemonHolder;
	PokedexData m_PokedexData;
private:
	float xPos = 320.f;
	float yPos = 160.f;
	int xMoveAmount = 0;
	int yMoveAmount = 0;
	float m_MoveSpeed = 0.25f;
	float m_TurnSpeed = 0.01f;
	float m_OldXPos = 0.f;
	float m_OldYPos = 0.f;
	float m_TempXPos = 0.f;
	float m_TempYPos = 0.f;
	float m_Timer = 0.f;
	bool m_hasAnimated = false;
	bool m_IsLeftAnim = false;
	bool m_IsTurning = false;
	std::string m_TexturePath = "C:/Users/thijs/Documents/projectpoke/ProjectPoke/Data/Sprites/Player/Player.png";
	sf::Texture m_TileTexture;
	sf::Vector2<int> m_TextureXY = sf::Vector2<int>(32, 32);
	sf::Vector2<int> m_TextureWidthHeight = sf::Vector2<int>(32,32);

};