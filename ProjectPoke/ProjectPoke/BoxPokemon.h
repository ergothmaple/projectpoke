#pragma once
#include "Pokemon.h"
#include <vector>
class BoxPokemon
{
public:
	BoxPokemon();
	~BoxPokemon();

private:
	std::vector<Pokemon> m_BoxPokemon;

};