#include "EntityManager.h"
#include "ManagerManager.h"
#include "ScriptEventManager.h"
EntityManager::EntityManager()
{
}

EntityManager::~EntityManager()
{
}

e_ManagerType EntityManager::getManagerType()
{
    return e_ManagerType::EntityManager;
}

void EntityManager::DrawMap(sf::RenderWindow& a_Window, sf::View& a_Camera)
{
    a_Camera.setCenter(player.getTempPos().x + 16.f, player.getTempPos().y + 48.f);
    a_Window.setView(a_Camera);
    for (int i = 0; i < m_Npcs.size(); i++)
    {
        a_Window.draw(m_Npcs[i]->m_Sprite);
    }
    a_Window.draw(player.m_TileSprite);
}

void EntityManager::Initialize()
{
    player.LoadPlayerSprite();
}

void EntityManager::Update(float a_DeltaTime)
{
    player.Update(a_DeltaTime);
}

void EntityManager::AddNpcByID(int a_ID, int a_xPos, int a_yPos)
{
    Npc* npc = new Npc;
    npc->m_ID = a_ID;
    npc->m_xPos = a_xPos * 32.f;
    npc->m_yPos = a_yPos * 32.f - 32.f;
    m_NpcLoader->LoadNpcData(npc);
    npc->Initialize();
    m_Npcs.push_back(npc);
}

void EntityManager::RunNpcScriptByID(int a_ID)
{
    for (int i = 0; i < m_Npcs.size(); i++)
    {
        if (m_Npcs[i]->m_ID == a_ID)
        {
            if (player.m_MovementDirection == MovementDirection::Right)
            {
                m_Npcs[i]->SetDirection(MovementDirection::Left);
            }
            else if (player.m_MovementDirection == MovementDirection::Left)
            {
                m_Npcs[i]->SetDirection(MovementDirection::Right);
            }
            else if (player.m_MovementDirection == MovementDirection::Up)
            {
                m_Npcs[i]->SetDirection(MovementDirection::Down);
            }
            else if (player.m_MovementDirection == MovementDirection::Down)
            {
                m_Npcs[i]->SetDirection(MovementDirection::Up);
            }
            static_cast<ScriptEventManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::ScriptEventManager)[0])->StartScript(m_Npcs[i]->m_ScriptName);
            return;
        }
    }
}
