// ProjectPoke.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "application.h"

int main()
{
    Application application;
    application.Initialize();
    application.Run();

}