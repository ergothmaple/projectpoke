#pragma once
#include "ManagerEnum.h"
#include "BaseManager.h"
#include <SFML/Graphics.hpp>
class InputManager : public BaseManager
{
public:
	InputManager();
	~InputManager();
	e_ManagerType getManagerType();
	void Initialize();
	void ProcessInput();
	void ProcessKeyReleased(sf::Event m_Event);
protected:
private:
};