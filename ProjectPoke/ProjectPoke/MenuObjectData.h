#pragma once
#include <SFML/System/Vector2.hpp>
class MenuObjectData
{
public:
	MenuObjectData();
	~MenuObjectData();
	sf::Vector2f m_Scale;
	sf::Vector2f m_LeftUpPosition;
	sf::Vector2f m_RightDownPosition;
	sf::Vector2f m_LeftUpTexCoord;
	sf::Vector2f m_RightDownTexCoord;

private:
};
