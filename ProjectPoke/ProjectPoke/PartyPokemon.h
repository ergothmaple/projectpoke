#pragma once
#include "Pokemon.h"
#include <vector>

class PartyPokemon
{
public:
	PartyPokemon();
	~PartyPokemon();
	void AddPokemon(Pokemon* a_Pokemon);
	void LoadAndDrawPokemon();
	void UpdatePokemon(float a_DeltaTime);
	std::vector<Pokemon*> GetPartyPokemon();
private:
	std::vector<Pokemon*> m_PartyPokemon;

};