#include "Application.h"
#include "MapManager.h"
#include "InputManager.h"
#include "ManagerManager.h"
#include "EntityManager.h"
#include "ScriptEventManager.h"
#include "ScriptEventReader.h"
#include "DialogueManager.h"
#include "PokemonManager.h"
#include "MenuUIManager.h"
Application::Application()
{
}

Application::~Application()
{
}

void Application::Initialize()
{
    ManagerManager::GetInstance()->addManager(new InputManager);
    ManagerManager::GetInstance()->addManager(new MapManager);
    ManagerManager::GetInstance()->addManager(new EntityManager);
    ManagerManager::GetInstance()->addManager(new ScriptEventManager);
    ManagerManager::GetInstance()->addManager(new DialogueManager);
    ManagerManager::GetInstance()->addManager(new PokemonManager);
    ManagerManager::GetInstance()->addManager(new MenuUIManager);
    ManagerManager::GetInstance()->InitializeManagers();
}

void Application::Update(float a_DeltaTime)
{
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->Update(a_DeltaTime);
    static_cast<InputManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::InputManager).at(0))->ProcessInput();
}

void Application::Draw(sf::RenderWindow& a_Window, sf::View& a_Camera)
{
    static_cast<MapManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MapManager).at(0))->DrawMap(a_Window);
    static_cast<EntityManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::EntityManager).at(0))->DrawMap(a_Window, a_Camera);
    static_cast<MenuUIManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::MenuUIManager).at(0))->DrawPartyMenu(a_Window, a_Camera);

}

void Application::Run()
{
    sf::RenderWindow window(sf::VideoMode(256 * 2, 192 * 2), "ProjectPoke");
    sf::Clock clock;
    deltaTime = clock.restart().asSeconds();
    sf::View camera(sf::Vector2f(320.f, 64.f), sf::Vector2f(256 * 2, 192 * 2));
    window.setView(camera);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyReleased)
            {
                static_cast<InputManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::InputManager).at(0))->ProcessKeyReleased(event);

            }
        }
        //Update
        Update(deltaTime);

        //Draw the current map
        window.clear();



        //Application::Draw()
        Draw(window, camera);
        static_cast<DialogueManager*>(ManagerManager::GetInstance()->getManagersOfType(e_ManagerType::DialogueManager).at(0))->DisplayText(window, camera, deltaTime);
        window.display();


        deltaTime = clock.restart().asSeconds();
    }
}
