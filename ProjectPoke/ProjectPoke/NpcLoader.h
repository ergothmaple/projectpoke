#pragma once
#include "Npc.h"
class NpcLoader
{
public:
	NpcLoader();
	~NpcLoader();
	//Load npc data from file with pointer to npc as arg so you can set data with help of pointer
	void LoadNpcData(Npc* a_Npc);
private:

};
