#pragma once
#include <vector>
#include <string>

/**
 * \brief Contains the actual definitions of dynamic functions that can be scripted.
 */
class ScriptableFunctions
{
public:
	//args: int xAmount
	static void MovePlayerX(std::vector<std::string> args);
	//args: int yAmount
	static void MovePlayerY(std::vector<std::string> args);
	//args: string Dialogue
	static void SayDialogue(std::vector<std::string> args);
	//args: int OptionAmount, string dialogue
	//Option indicated by #1#Option#2#Option  
	static void SayOptionDialogue(std::vector<std::string> args);

	static void SetVariable(std::vector<std::string> args);

	static void GivePokemon(std::vector<std::string> args);
};