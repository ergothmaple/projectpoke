#pragma once
#include <string>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "SFML/Graphics/RenderWindow.hpp"
class Pokemon
{
public:
	Pokemon();
	~Pokemon();
	void LoadPokemonSprite();
	void InitializeData();
	void Update(float a_DeltaTime);
	void DrawIcon(sf::Vector2f a_Position, sf::RenderWindow& a_Window);
public:
	int m_ID;
	std::string m_Name;
	std::string m_NickName = "";
	int m_Level;
	int m_MaxHP;
	int m_CurHP;
	int m_Gender;
	std::string m_BoxTexturePath;
	sf::Texture m_BoxTexture;
	sf::Sprite m_BoxSprite;
private:
	int m_BoxSpriteAnimation = 0;
	float m_CurrentUpdate = 0.f;
	float m_Countdown = 0.5f;
};